dune_install(basisfunctionset.hh default.hh functor.hh
             proxy.hh simple.hh transformation.hh
             tuple.hh vectorial.hh)

dune_add_subdirs(test)
