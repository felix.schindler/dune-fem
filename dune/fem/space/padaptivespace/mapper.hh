#ifndef DUNE_FEM_SPACE_PADAPTIVE_MAPPER_HH
#define DUNE_FEM_SPACE_PADAPTIVE_MAPPER_HH

#include <dune/common/exceptions.hh>

#include <dune/geometry/type.hh>
#include <dune/geometry/typeindex.hh>

#include <dune/grid/utility/persistentcontainer.hh>

#include <dune/fem/misc/capabilities.hh>
#include <dune/fem/misc/metaprogramming.hh>
#include <dune/fem/space/common/basesetlocalkeystorage.hh>
#include <dune/fem/space/common/dofmanager.hh>
#include <dune/fem/space/lagrange/lagrangepoints.hh>
#include <dune/fem/space/mapper/codimensionmapper.hh>
#include <dune/fem/space/mapper/dofmapper.hh>
#include <dune/fem/space/mapper/genericadaptivedofmapper.hh>


namespace Dune
{

  namespace Fem
  {

    // Internal forward declaration
    // ----------------------------
    template< class GridPart, int polOrder >
    class PAdaptiveLagrangeMapper;



    // PAdaptiveLagrangeMapperTraits
    // -----------------------------

    template< class GridPart, int polOrder >
    struct PAdaptiveLagrangeMapperTraits
    {
      typedef GridPart GridPartType;

      static const int polynomialOrder = polOrder;
      // if this is set to true the mapper behaves like a DG mapper
      static const bool discontinuousMapper = false;

      typedef typename GridPartType::template Codim< 0 >::EntityType ElementType;
      typedef PAdaptiveLagrangeMapper< GridPartType, polynomialOrder > DofMapperType;

      //! type of the compiled local key
      typedef LagrangePointSet< GridPartType, polynomialOrder >  CompiledLocalKeyType;
      typedef BaseSetLocalKeyStorage< CompiledLocalKeyType > BaseSetLocalKeyStorageType;

      typedef std::vector< BaseSetLocalKeyStorageType > CompiledLocalKeyVectorType ;

      typedef int SizeType ;
      typedef int GlobalKeyType ;
    };



    // First Order Lagrange Mapper
    // ---------------------------

#if 0
    template< class GridPart >
    class PAdaptiveLagrangeMapper< GridPart, 1 >
    : public CodimensionMapper< GridPart, GridPart::GridType::dimension >
    {
      typedef PAdaptiveLagrangeMapper< GridPart, 1 > ThisType;
      typedef CodimensionMapper< GridPart, GridPart::GridType::dimension > BaseType;

    public:
      //! type of the grid part
      typedef typename BaseType::GridPartType GridPartType;

      //! type of entities (codim 0)
      typedef typename BaseType::ElementType ElementType;

      //! type of the underlying grid
      typedef typename GridPartType::GridType GridType;

      //! type of coordinates within the grid
      typedef typename GridType::ctype FieldType;

      //! dimension of the grid
      static const int dimension = GridType::dimension;

      //! order of the Lagrange polynoms
      static const int polynomialOrder = 1;

      // my traits class
      typedef PAdaptiveLagrangeMapperTraits< GridPart, polynomialOrder > Traits;

      typedef typename Traits :: CompiledLocalKeyVectorType  CompiledLocalKeyVectorType;

    public:
      //! constructor
      PAdaptiveLagrangeMapper ( const GridPartType &gridPart, CompiledLocalKeyVectorType& )
      : BaseType( gridPart )
      {}

      bool fixedDataSize ( const int codim ) const
      {
        return true;
      }

      int polynomOrder( const ElementType& entity ) const
      {
        return 1;
      }

      void setPolynomOrder( const ElementType& entity, const int polOrd )
      {
      }

      void adapt() {}
    };
#endif


    // Higher Order Lagrange Mapper
    // ----------------------------

    template< class GridPart, int polOrder >
    class PAdaptiveLagrangeMapper
      : public GenericAdaptiveDofMapper< PAdaptiveLagrangeMapperTraits< GridPart, polOrder > >
    {
    public:
      // my traits class
      typedef PAdaptiveLagrangeMapperTraits< GridPart, polOrder > Traits;

    private:
      typedef PAdaptiveLagrangeMapper< GridPart, polOrder > ThisType;
      typedef GenericAdaptiveDofMapper< Traits > BaseType;

    public:
      //! type of the grid part
      typedef typename Traits::GridPartType GridPartType;

      //! type of compiled local keys vector
      typedef typename Traits :: CompiledLocalKeyVectorType  CompiledLocalKeyVectorType;

    public:
      //! constructor
      PAdaptiveLagrangeMapper ( const GridPartType &gridPart,
                                CompiledLocalKeyVectorType &compiledLocalKeys )
        : BaseType( gridPart, compiledLocalKeys )
      {
      }

      //! sort of copy constructor
      PAdaptiveLagrangeMapper ( const ThisType& other,
                                CompiledLocalKeyVectorType &compiledLocalKeys )
        : BaseType( other, compiledLocalKeys )
      {}
    };

    template< class GridPart, int polOrder >
    class PAdaptiveDGMapper;

    template< class GridPart, int polOrder >
    struct PAdaptiveDGMapperTraits
      : public PAdaptiveLagrangeMapperTraits< GridPart, polOrder >
    {
      // this is a mapper for DG
      static const bool discontinuousMapper = true ;

      typedef typename GridPart::template Codim< 0 >::EntityType ElementType;
      typedef PAdaptiveDGMapper< GridPart, polOrder > DofMapperType;
      typedef int SizeType ;
      typedef int GlobalKeyType ;
    };


    // Higher Order Adaptive DG Mapper
    // -------------------------------

    template< class GridPart, int polOrder >
    class PAdaptiveDGMapper
      : public GenericAdaptiveDofMapper< PAdaptiveDGMapperTraits< GridPart, polOrder > >
    {
    public:
      // my traits class
      typedef PAdaptiveDGMapperTraits< GridPart, polOrder > Traits;

    private:
      typedef PAdaptiveDGMapper< GridPart, polOrder > ThisType;
      typedef GenericAdaptiveDofMapper< Traits > BaseType;

    public:
      //! type of the grid part
      typedef typename Traits::GridPartType GridPartType;

      //! type of compiled local keys vector
      typedef typename Traits :: CompiledLocalKeyVectorType  CompiledLocalKeyVectorType;

    public:
      //! constructor
      PAdaptiveDGMapper ( const GridPartType &gridPart,
                          CompiledLocalKeyVectorType &compiledLocalKeys )
        : BaseType( gridPart, compiledLocalKeys )
      {}

      //! sort of copy constructor
      PAdaptiveDGMapper ( const ThisType& other,
                          CompiledLocalKeyVectorType &compiledLocalKeys )
        : BaseType( other, compiledLocalKeys )
      {}
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_SPACE_PADAPTIVE_MAPPER_HH
